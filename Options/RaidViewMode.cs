namespace OptionsModule
{
    public enum RaidViewMode
    {
        DamageDone = 0,
        HealingDone,
        DamageReceived,
        Opponents
    }
}