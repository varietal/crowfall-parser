using System;
using System.Windows.Input;
using Prism.Mvvm;

namespace OptionsModule
{
    public class Hotkey : BindableBase
    {
        private Key _key;
        private ModifierKeys _modifierKeys;

        public Key Key
        {
            get => _key;
            private set => SetProperty(ref _key, value);
        }

        public ModifierKeys ModifierKeys
        {
            get => _modifierKeys;
            private set => SetProperty(ref _modifierKeys, value);
        }

        public Hotkey(Key key, ModifierKeys modifierKeys)
        {
            Key = key;
            ModifierKeys = modifierKeys;
        }

        public override string ToString()
        {
            return $"{ModifierKeys}+{Key}";
        }

        public static Hotkey Parse(string hotkeyString)
        {
            if (string.IsNullOrEmpty(hotkeyString))
                return null;

            var split = hotkeyString.Split('+');

            return Enum.TryParse(split[1], true, out Key key) && Enum.TryParse(split[0], true, out ModifierKeys modifierKeys)
                ? new Hotkey(key, modifierKeys)
                : null;
        }
    }
}