namespace OptionsModule
{
    public static class Hotkeys
    {
        public const string AfkNotifier = "Toggle AFK Notifier";
        public const string Windows = "Show/Hide Parser windows";
        public const string More = "Toggle ⮟ More";
        public const string Raid = "Toggle ⮟ Raid";
    }
}