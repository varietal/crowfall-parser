using System;
using System.IO;
using System.Windows;
using IniParser;
using IniParser.Model;

namespace OptionsModule
{
    public class IniParserService : IOptionsService
    {
        private readonly FileIniDataParser _parser;
        private IniData _data;
        private readonly string _filePath;

        public IniParserService()
        {
            _parser = new FileIniDataParser();

            var directoryPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Crowfall Parser";
            Directory.CreateDirectory(directoryPath);

            _filePath = directoryPath + @"\settings.ini";
            ReadSettings();
        }

        private void ReadSettings()
        {
            if (!File.Exists(_filePath))
            {
                using (var stream = File.Create(_filePath))
                {
                    stream.Close();
                }
            }

            try
            {
                _data = _parser.ReadFile(_filePath);
            }
            catch
            {
                File.Delete(_filePath);
                ReadSettings();
                MessageBox.Show("Settings file is corrupted. Settings were reset.");
            }
        }

        public void Set(string section, string property, string value)
        {
            _data[section][property] = value;
            _parser.WriteFile(_filePath, _data);
        }

        public string Get(string section, string property, string defaultValue)
        {
            var value = _data[section][property];
            return value ?? defaultValue;
        }
    }
}