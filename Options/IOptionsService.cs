namespace OptionsModule
{
    public interface IOptionsService
    {
        void Set(string section, string property, string value);
        string Get(string section, string property, string defaultValue);
    }
}