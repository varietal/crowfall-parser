using Prism.Mvvm;

namespace OptionsModule
{
    public class HotkeyBinding : BindableBase
    {
        private Hotkey _hotkey;
        private string _name;

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name , value);
        }

        public Hotkey Hotkey
        {
            get => _hotkey;
            set => SetProperty(ref _hotkey, value);
        }

        public HotkeyBinding(string name, Hotkey hotkey)
        {
            Name = name;
            Hotkey = hotkey;
        }
    }
}