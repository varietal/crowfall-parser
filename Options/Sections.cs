namespace OptionsModule
{
    public static class Sections
    {
        public static string Ui => "UI";
        public static string Parser => "Parser";
        public static string AfkNotifier => "AfkNotifier";
        public static string Raid => "Raid";
        public static string Hotkey => "Hotkey";
        public static string Support => "Support";
    }
}