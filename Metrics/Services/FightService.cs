using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Metrics.Commands;
using Metrics.Controls;
using Metrics.Handlers;
using Metrics.Models;
using Metrics.Properties;
using NLog;
using OptionsModule;

namespace Metrics.Services
{
    public sealed class FightService : INotifyPropertyChanged
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static readonly Subject<PaintMode> Repaint = new Subject<PaintMode>();
        private readonly ClientService _clientService;
        private readonly List<Character> _characters = new List<Character>();
        private Character _currentCharacter;
        private Regex _regex;
        private Dictionary<string, string> _context; // todo: replace with variables for performance?

        private readonly Dictionary<string, Regex> _regexes = new Dictionary<string, Regex>
        {
            {"en", new Regex(@"(.*) INFO    COMBAT    - Combat _\|\|_ Event=\[(Your|\w*)(.*) (hit|healed|whiffed) (You|.*).*for (\d*)(.*)\.\]")},
            {"ru", new Regex("(.*) INFO    COMBAT    - Combat _\\|\\|_ Event=\\[(Ваш|.*): \"(.*)\" (поражает цель|исцеляет цель|whiffed) (Игрок|.*) на (\\d*)(.*)\\.\\]")}, // todo: whiffed? 
        };

        private string _language;

        private readonly Dictionary<string, Dictionary<string, string>> _contexts = new Dictionary<string, Dictionary<string, string>>
        {
            {
                "en", new Dictionary<string, string>
                {
                    {"healed", "healed"},
                    {"Unknown", "Unknown"},
                    {"Lifesteal", "Lifesteal"},
                    {"Your", "Your"},
                    {"Fall", "Fall"},
                    {"Berserk", "Berserk Crush"},
                    {"Essence Burn", "Essence Burn"},
                    {"You", "You"},
                    {"(Critical)", "(Critical)"},
                    {"absorbed", "absorbed"},
                    {"damage", "damage"},
                }
            },
            {
                "ru", new Dictionary<string, string>
                {
                    {"healed", "исцеляет цель"},
                    {"Unknown", "Unknown"}, // todo: translate
                    {"Lifesteal", "Lifesteal"},
                    {"Your", "Ваш"},
                    {"Fall", "Падение"},
                    {"Berserk", "Berserk Crush"},
                    {"Essence Burn", "Essence Burn"},
                    {"You", "Игрок"},
                    {"(Critical)", "(Критический удар)"},
                    {"absorbed", "поглощает"},
                    {"damage", "урон"},
                }
            }
        };

        private static int _idleDuration = Options.Inst.IdleDuration;
        private readonly Subject<string> _sendStats;

        public Character CurrentCharacter
        {
            get => _currentCharacter;
            set
            {
                _currentCharacter = value;
                OnPropertyChanged(nameof(CurrentCharacter));
                OnPropertyChanged(nameof(CurrentFight));
                OnPropertyChanged(nameof(FightsCount));
            }
        }

        public Fight CurrentFight => CurrentCharacter?.CurrentFight;
        public int FightsCount => CurrentCharacter?.Fights.Count ?? 0;

        public string RaidName { get; set; }
        public Fight RaidFight { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public FightService(ClientService clientService)
        {
            _sendStats = new Subject<string>();
            _sendStats.Sample(TimeSpan.FromSeconds(1)).Subscribe(async next =>
            {
                await _clientService.Send(new ClientStatsCommand(RaidFight, RaidName));
                RaidFight.SyncTime = clientService.SyncTime;
            });

            _clientService = clientService;
            _clientService.RaidStarted += SolveRaidFight;
            _clientService.RaidStopped += StopRaidFight;

            Options.Inst.PropertyChanged += (sender, args) =>
            {
                switch (args.PropertyName)
                {
                    case nameof(Options.Inst.IdleDuration):
                        _idleDuration = Options.Inst.IdleDuration;
                        break;
                    case nameof(Options.Inst.DamageDetailsSortingDirection):
                    case nameof(Options.Inst.DamageDetailsSortingProperty):
                        Fight.EnsureComparison(CurrentFight.DamageDoneRecords, Options.Inst.DamageDetailsSortingDirection, Options.Inst.DamageDetailsSortingProperty);
                        Fight.EnsureComparison(CurrentFight.DamageReceivedRecords, Options.Inst.DamageDetailsSortingDirection, Options.Inst.DamageDetailsSortingProperty);
                        break;
                    case nameof(Options.Inst.HealingDetailsSortingDirection):
                    case nameof(Options.Inst.HealingDetailsSortingProperty):
                        Fight.EnsureComparison(CurrentFight.HealingDoneRecords, Options.Inst.HealingDetailsSortingDirection, Options.Inst.HealingDetailsSortingProperty);
                        Fight.EnsureComparison(CurrentFight.HealingReceivedRecords, Options.Inst.HealingDetailsSortingDirection, Options.Inst.HealingDetailsSortingProperty);
                        break;
                    case nameof(Options.Inst.OpponentsSortingProperty):
                    case nameof(Options.Inst.OpponentsSortingDirection):
                        Fight.EnsureComparison(CurrentFight.Opponents, Options.Inst.OpponentsSortingDirection, Options.Inst.OpponentsSortingProperty);
                        break;
                }
            };
        }


        private void StopRaidFight(object sender, EventArgs e)
        {
            RaidFight = null;
            Logger.Info("RAID FIGHT STOPPED");
        }

        public void ParseLine(CombatLog log, string line, bool catchUp, bool recalculate = false)
        {
            if (_language == null && !DetectLanguage(line))
                return; // todo: notify unsupported language

            var match = _regex.Match(line);
            if (!match.Success)
                return;

            var timestamp = DateTime.Parse(match.Groups[1].Value);

            Fight currentFight = null;
            if (!catchUp)
            {
                currentFight = SolveCurrentFight(log, timestamp);

                if (currentFight == null)
                    return;
            }

            var tail = match.Groups[7].Value;
            var amount = int.Parse(match.Groups[6].Value);
            if (amount == 0 && !tail.Contains(_context["absorbed"]))
                return;

            var type = "hit points";
            if (tail.Contains(_context["damage"]))
            {
                type = GetPreviousWord(tail, _context["damage"]);
            }

            var damage = match.Groups[4].Value != _context["healed"];
            var skillName = match.Groups[3].Value.Trim();
            var skill = string.IsNullOrEmpty(skillName)
                ? damage
                    ? type == "Hunger"
                        ? "Hunger"
                        : _context["Unknown"]
                    : _context["Lifesteal"]
                : skillName;
            var character = match.Groups[2].Value.Trim();

            var target = (string.IsNullOrEmpty(match.Groups[5].Value) ? _context["Unknown"] : match.Groups[5].Value).Trim();
            var me = target == _context["You"];

            var absorbed = 0;
            if (tail.Contains(_context["absorbed"]))
            {
                int.TryParse(GetPreviousWord(tail, _context["absorbed"]), out absorbed);
                if (absorbed != 0)
                    amount += absorbed;
            }

            var critical = tail.Contains(_context["(Critical)"]);
            var selfDamage = skill == _context["Fall"] || skill == _context["Berserk"] || skill == _context["Essence Burn"] || type == "Hunger";
            var mine = character == _context["Your"] && !selfDamage;

            if (!catchUp)
                currentFight.Update(mine, damage, skill, selfDamage ? skill : (mine ? target : character), amount, absorbed, critical, me, selfDamage, type);

            if (RaidFight == null || recalculate)
                return;

            if (!catchUp)
            {
                RaidFight.LastAction = timestamp;
                RaidFight.BufferLine(log, line, timestamp);
            }

            RaidFight.Update(mine, damage, skill, selfDamage ? skill : (mine ? target : character), amount, absorbed, critical, me, selfDamage, type);
            _sendStats.OnNext("");

            if (RaidFight.DamageDone?.PerSecond < 0 || RaidFight.HealingDone?.PerSecond < 0 || RaidFight.DamageReceived?.PerSecond < 0 || RaidFight.HealingReceived?.PerSecond < 0)
            {
                Logger.Warn($"NEGATIVE PER SECOND. DURATION: {RaidFight.Duration}. STARTED: {RaidFight.Started}. " +
                            $"LAST ACTION: {RaidFight.LastAction}. SYNC TIME (LOCAL - SERVER): {RaidFight.SyncTime}");
            }
        }

        public static string GetPreviousWord(string search, string word)
        {
            const string stops = " (";
            var index = search.IndexOf(word, StringComparison.Ordinal);
            var previous = string.Empty;
            for (var i = index - 2; i >= 0 && !stops.Contains(search[i]); i--)
            {
                previous = search[i] + previous;
            }

            return previous;
        }

        private bool DetectLanguage(string line)
        {
            foreach (var regex in _regexes.Where(regex => regex.Value.Match(line).Success))
            {
                _regex = regex.Value;
                _language = regex.Key;
                _context = _contexts[_language];
                return true;
            }

            return false;
        }

        private Fight SolveCurrentFight(CombatLog log, DateTime timestamp)
        {
            var character = _characters.FirstOrDefault(x => x.Log == log);

            if (character == null)
            {
                character = new Character(log);
                _characters.Add(character);
            }

            var fights = character.Fights;
            if (character.CurrentFight == null)
            {
                character.CurrentFight = new Fight {Started = timestamp, Index = fights.Count + 1};
                character.CurrentFight.EnsureComparisons();
                fights.Add(character.CurrentFight);
            }
            else if (timestamp - character.CurrentFight.LastAction > TimeSpan.FromSeconds(_idleDuration))
            {
                var i = fights.IndexOf(character.CurrentFight);
                while (i < fights.Count && ShouldStartNewFight(timestamp, fights[i].LastAction))
                {
                    i++;
                }

                if (i == fights.Count)
                {
                    character.CurrentFight = new Fight {Started = timestamp, Index = fights.Count + 1};
                    character.CurrentFight.EnsureComparisons();
                    fights.Add(character.CurrentFight);
                }
                else
                {
                    character.CurrentFight = fights[i];
                }
            }

            character.CurrentFight.LastAction = timestamp;
            CurrentCharacter ??= character;

            return character.CurrentFight;
        }

        public static bool ShouldStartNewFight(DateTime? newLastAction, DateTime? oldLastAction)
        {
            return newLastAction - oldLastAction > TimeSpan.FromSeconds(_idleDuration);
        }

        public void First()
        {
            if (CurrentFight != null)
                _currentCharacter.CurrentFight = _currentCharacter.Fights.FirstOrDefault();

            OnPropertyChanged(nameof(CurrentFight));
        }

        public void Last()
        {
            if (CurrentFight != null)
                _currentCharacter.CurrentFight = _currentCharacter.Fights.LastOrDefault();

            OnPropertyChanged(nameof(CurrentFight));
        }

        public void Next()
        {
            if (CurrentFight == null)
                return;

            var fights = _currentCharacter.Fights;
            var nextFight = fights.ElementAtOrDefault(fights.IndexOf(CurrentFight) + 1);

            if (nextFight != null)
                _currentCharacter.CurrentFight = nextFight;

            OnPropertyChanged(nameof(CurrentFight));
        }

        public void Previous()
        {
            if (CurrentFight == null)
                return;

            var fights = _currentCharacter.Fights;
            var previousFight = fights.ElementAtOrDefault(fights.IndexOf(CurrentFight) - 1);

            if (previousFight != null)
                _currentCharacter.CurrentFight = previousFight;

            OnPropertyChanged(nameof(CurrentFight));
        }

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Reset()
        {
            _currentCharacter = null;
            _characters.Clear();
        }

        public void SelectCharacter(object sender, EventArgs args)
        {
            var bestActivity = -1;
            Character mostActiveCharacter = null;
            var tenSecondsAgo = DateTime.Now.Subtract(TimeSpan.FromSeconds(10));
            foreach (var character in _characters)
            {
                var fights = character.Fights.Where(x => x.LastAction > tenSecondsAgo);
                var activity = fights.Select(x => (x.DamageDone?.Total ?? 0) + (x.HealingDone?.Total ?? 0)).Sum();
                if (activity > bestActivity)
                {
                    bestActivity = activity;
                    mostActiveCharacter = character;
                }
            }

            if (bestActivity > 0)
                CurrentCharacter = mostActiveCharacter;
        }

        private void SolveRaidFight(RaidStartedHandlerArgs args)
        {
            try
            {
                if (RaidFight != null && RaidFight.StartedOnServer == args.StartedOnServer)
                    return;

                var oldRaidFight = RaidFight;
                RaidFight = new Fight {Started = args.Started, StartedOnServer = args.StartedOnServer};

                if (oldRaidFight == null)
                    return;

                // catch up on records from previous fight, if they belong to this fight too
                foreach (var (log, line, timestamp) in oldRaidFight.Lines)
                {
                    if (timestamp >= args.Started)
                    {
                        ParseLine(log, line, true);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "FAILED TO SOLVE RAID FIGHT");
            }
        }
    }
}