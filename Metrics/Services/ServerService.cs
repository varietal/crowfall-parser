using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using Metrics.Commands;
using Metrics.Extensions;
using Metrics.Models;
using OptionsModule;

namespace Metrics.Services
{
    public sealed class ServerService : RaidService
    {
        public override string ServiceType => "SERVER";

        private ConcurrentDictionary<string, RaidCharacter> _raidCharacters;
        private DateTime _raidStarted;
        private DateTime _raidLastAction;
        private Subject<string> _sendStats;
        private Timer _keepAliveTimer;
        private const int UploadStatsIntervalSecs = 60;
        public readonly List<Receiver> Receivers = new List<Receiver>();
        private Timer _uploadStatsTimer;
        public int RaidStatsSent { get; private set; }

        public override void Start(WebService webService)
        {
            _raidStarted = DateTime.Now;
            _raidLastAction = DateTime.Now;
            _raidCharacters = new ConcurrentDictionary<string, RaidCharacter>();

            _sendStats = new Subject<string>();
            _sendStats.Sample(TimeSpan.FromSeconds(1)).Subscribe(async next =>
            {
                await Broadcast(new RaidStatsCommand(_raidCharacters.Values));
                RaidStatsSent++;
            });

            _keepAliveTimer = new Timer(async sender =>
            {
                try
                {
                    foreach (var receiver in Receivers.Where(receiver => receiver.LostConnection()))
                    {
                        RemoveReceiver(receiver.ToUdpEndPoint());
                        await Send(receiver, new ReconnectCommand());
                    }

                    await SendCommand(Receivers.Where(x => x.ConnectedByUdp && !x.IsLoopback()).ToList(), new KeepAliveCommand());
                }
                catch (Exception e)
                {
                    // todo: check if we need to stop timer
                    ((Timer) sender).Dispose();
                    LogError(e, "EXCEPTION SENDING KEEP ALIVE");
                }
            });
            _keepAliveTimer.Change(TimeSpan.Zero, TimeSpan.FromSeconds(KeepAliveIntervalSecs));

            _uploadStatsTimer = new Timer(async sender => await UploadRaidStats());
            _uploadStatsTimer.Change(TimeSpan.Zero, TimeSpan.FromSeconds(UploadStatsIntervalSecs));

            base.Start(webService);
        }


        public async Task UploadRaidStats()
        {
            try
            {
                // todo: bad dependency flow
                if (WebRelay != null)
                {
                    var command = new RaidStatsCommand(_raidCharacters.Values.ToArray());
                    await WebRelay.UploadRaidStats(_raidStarted, _raidLastAction, command.Opponents.Name(), command.Characters.ToArray());
                }
            }
            catch (Exception e)
            {
                LogError(e, "EXCEPTION SENDING RAID STATS TO SERVER");
            }
        }

        public override void Reset()
        {
            _raidCharacters?.Clear();
            _sendStats?.Dispose();
            _keepAliveTimer?.Dispose();
            _uploadStatsTimer?.Dispose();
            Receivers?.Clear();
        }

        public override async Task Process(Command command, IPEndPoint endPoint, bool byUdp)
        {
            Receiver receiver;

            switch (command)
            {
                case KeepAliveCommand _:
                    if (!byUdp)
                        return;

                    receiver = GetReceiver(endPoint);
                    if (receiver != null)
                        receiver.AliveAt = DateTime.Now;
                    break;
                case PunchingCommand punchingCommand:
                    await AddReceiver(endPoint, punchingCommand.Punched, byUdp);
                    break;
                case JoinCommand joinCommand:
                    receiver = await AddReceiver(endPoint, endPoint.IsLoopback(), byUdp);
                    await Send(receiver, joinCommand.ProtocolVersion == ProtocolVersion ? (Command) new RaidCommand(_raidStarted) : new IncompatibleProtocolCommand(ProtocolVersion));
                    break;
                case ClientStatsCommand clientStatsCommand:
                    try
                    {
                        var lastAction = clientStatsCommand.RaidFight?.LastAction;
                        if (lastAction == null)
                        {
                            LogWarn($"RECEIVED NO-ACTION CLIENT STATS FROM: {clientStatsCommand.RaidName}.");
                            return;
                        }

                        if (lastAction > _raidLastAction && lastAction < DateTime.Now + TimeSpan.FromSeconds(Options.Inst.IdleDuration)) // Note: action is in adequate time window
                        {
                            if (FightService.ShouldStartNewFight(lastAction, _raidLastAction))
                            {
                                await UploadRaidStats();
                                _raidCharacters = new ConcurrentDictionary<string, RaidCharacter>();
                                _raidStarted = lastAction.Value;
                                await Broadcast(new RaidCommand(_raidStarted));
                                _raidLastAction = lastAction.Value;
                            }
                            else
                            {
                                _raidLastAction = lastAction.Value;

                                var fromCharacter = clientStatsCommand.ToRaidCharacter();
                                _raidCharacters.AddOrUpdateSync(fromCharacter.Name, name => fromCharacter, (name, x) =>
                                {
                                    x.Fight = fromCharacter.Fight;
                                    return x;
                                });

                                _sendStats.OnNext("");
                            }
                        }
                        else
                        {
                            LogWarn($"RECEIVED OUT-OF-SYNC CLIENT STATS: {lastAction.Value}. LAST RAID ACTION: {_raidLastAction}. IDLE: {Options.Inst.IdleDuration}. " +
                                    $"FROM: {clientStatsCommand.RaidName}.");
                        }
                    }
                    catch (Exception e)
                    {
                        LogError(e, "EXCEPTION HANDLING CLIENT STATS");
                    }

                    break;
            }
        }

        private async Task<Receiver> AddReceiver(IPEndPoint endPoint, bool connectedDirectly, bool byUdp)
        {
            var receiver = GetReceiver(endPoint);
            if (receiver == null)
            {
                receiver = new Receiver(endPoint, byUdp);
                Receivers.Add(receiver);
            }
            else
            {
                if (byUdp)
                    receiver.UdpPort = endPoint.Port;
                else
                    receiver.RelayPort = endPoint.Port;
            }

            if (connectedDirectly)
                receiver.ConnectedByUdp = true;

            var remoteReceivers = Receivers.Where(x => !x.IsLoopback()).ToList();
            if (remoteReceivers.Count > 4 && remoteReceivers.All(x => !x.ConnectedByUdp))
            {
                LogInfo("NAT BLOCKING CONNECTIONS, CHANGING SERVER");
                await WebRelay.ChangeServer();
            }

            return receiver;
        }

        private Receiver GetReceiver(IPEndPoint endPoint)
        {
            return Receivers.FirstOrDefault(x => Equals(x.Address, endPoint.Address) && (x.RelayPort == endPoint.Port || x.UdpPort == endPoint.Port));
        }

        public void RemoveReceiver(IPEndPoint endPoint)
        {
            var receiver = GetReceiver(endPoint);
            if (receiver != null)
            {
                Receivers.Remove(receiver);
            }
        }

        private async Task Broadcast(Command command)
        {
            await SendCommand(Receivers, command);
        }

        private async Task Send(Receiver receiver, Command command)
        {
            await SendCommand(new List<Receiver> {receiver}, command);
        }

        public async Task PunchHole(Computer computer)
        {
            try
            {
                Receiver receiver = null;
                for (var i = 0; i < PunchingTries; i++)
                {
                    receiver = GetReceiver(computer.EndPoint);
                    await Send(receiver ?? new Receiver(computer.EndPoint, true), new PunchingCommand(receiver != null));
                    LogInfo($"HOLE PUNCHING {receiver?.ToUdpEndPoint() ?? computer.EndPoint}");
                    await Task.Delay(i * 500);
                }

                if (receiver == null || !receiver.ConnectedByUdp)
                    LogInfo($"FAILED TO REACH CLIENT {computer.EndPoint}, USE RELAY");
            }
            catch (Exception e)
            {
                LogError(e, $"EXCEPTION CONNECTING TO CLIENT {computer.EndPoint}");
            }
        }
    }
}