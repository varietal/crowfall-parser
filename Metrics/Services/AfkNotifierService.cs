using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Windows.Threading;
using OptionsModule;

namespace Metrics.Services
{
    public class AfkNotifierService
    {
        private readonly NotificationService _notificationService;
        private const uint WinEventOutOfContext = 0;
        private const uint EventSystemForeground = 3;
        private static readonly Dictionary<IntPtr, DispatcherTimer> Timers = new Dictionary<IntPtr, DispatcherTimer>();
        private WinEventProc _listener;
        private IntPtr _winHook;

        private static bool _notifiedEnabled;

        private static DateTime _crowfallActivated = DateTime.Now;

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr SetWinEventHook(uint eventMin, uint eventMax, IntPtr hmodWinEventProc, WinEventProc lpfnWinEventProc, int idProcess, int idThread, uint dwflags);

        [DllImport("user32.dll")]
        private static extern int UnhookWinEvent(IntPtr hWinEventHook);

        public AfkNotifierService(NotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        private void OnWindowActivated(IntPtr hWinEventHook, uint iEvent, IntPtr hWnd, int idObject, int idChild, int dwEventThread, int dwmsEventTime)
        {
            if (Options.Inst.TopmostAlways || Process.GetProcessesByName("CrowfallClient").Any(x => x.MainWindowHandle == hWnd))
            {
                _crowfallActivated = DateTime.Now;
                Options.Inst.Topmost = true;
            }
            else if (DateTime.Now - _crowfallActivated > TimeSpan.FromMilliseconds(500))
            {
                Options.Inst.Topmost = false;
            }

            if (!_notifiedEnabled)
                return;

            if (Timers.ContainsKey(hWnd))
            {
                Timers[hWnd].Stop();
                Timers.Remove(hWnd);
            }

            var crowfallWindows = new List<IntPtr>();

            foreach (var process in Process.GetProcessesByName("CrowfallClient"))
            {
                var processHandle = process.MainWindowHandle;

                if (hWnd != processHandle)
                {
                    if (!Timers.ContainsKey(processHandle))
                    {
                        var timer = new DispatcherTimer {Interval = TimeSpan.FromMinutes(Options.Inst.NotifierTimer)};
                        Timers.Add(processHandle, timer);
                        timer.Start();
                        timer.Tick += (sender, args) =>
                        {
                            new SoundPlayer(AppDomain.CurrentDomain.BaseDirectory + @"\crow.wav").Play();
                            _notificationService.ShowWarning($"Your Crowfall Client was inactive for {Options.Inst.NotifierTimer} minutes!");
                            timer.Stop();
                        };
                    }
                }

                crowfallWindows.Add(processHandle);
            }

            var outdatedTimers = new List<IntPtr>();
            foreach (var timer in Timers)
            {
                if (!crowfallWindows.Contains(timer.Key))
                    outdatedTimers.Add(timer.Key);
            }

            foreach (var outdatedTimer in outdatedTimers)
            {
                Timers[outdatedTimer].Stop();
                Timers.Remove(outdatedTimer);
            }
        }

        public void StartListeningForWindowChanges()
        {
            _listener = OnWindowActivated;
            _winHook = SetWinEventHook(EventSystemForeground, EventSystemForeground, IntPtr.Zero, _listener, 0, 0, WinEventOutOfContext);
        }

        public void StopListeningForWindowChanges()
        {
            UnhookWinEvent(_winHook);
        }

        private delegate void WinEventProc(IntPtr hWinEventHook, uint iEvent, IntPtr hWnd, int idObject, int idChild, int dwEventThread, int dwmsEventTime);

        public void SetNotifyState(bool notifierEnabled)
        {
            _notifiedEnabled = notifierEnabled;
        }
    }
}