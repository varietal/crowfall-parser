using System.Collections.Generic;
using Metrics.Commands;
using Metrics.Models;

namespace Metrics.Services
{
    public class SentCommandHandlerArgs
    {
        public List<Receiver> Receivers { get; }
        public Command Command { get; }

        public SentCommandHandlerArgs(List<Receiver> receivers, Command command)
        {
            Receivers = receivers;
            Command = command;
        }
    }
}