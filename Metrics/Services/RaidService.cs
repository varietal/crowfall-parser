using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Metrics.Commands;
using Metrics.Models;
using NLog;

namespace Metrics.Services
{
    public abstract class RaidService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        protected const int ProtocolVersion = 2;
        protected const int PunchingTries = 6;
        protected const int KeepAliveIntervalSecs = 3;
        public abstract string ServiceType { get; }
        protected WebService WebRelay;
        public event SentCommandHandler SentCommand;

        public virtual void Start(WebService webService)
        {
            WebRelay = webService;
        }

        public abstract Task Process(Command command, IPEndPoint endPoint, bool byUdp);

        protected void LogInfo(string message)
        {
            Logger.Info($"{ServiceType}: {message}");
        }

        protected void LogError(Exception e, string message)
        {
            Logger.Error(e, $"{ServiceType}: {message}");
        }
        
        protected void LogWarn(string message)
        {
            Logger.Warn($"{ServiceType}: {message}");
        }

        protected async Task SendCommand(List<Receiver> receivers, Command command)
        {
            if (SentCommand != null)
                await SentCommand.Invoke(this, new SentCommandHandlerArgs(receivers, command));
        }

        public abstract void Reset();
    }
}