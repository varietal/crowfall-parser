﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using Metrics.ViewModels;

namespace Metrics.Views
{
    public partial class ExtendedMessageBox
    {
        // todo: refactor to properties

        public ExtendedMessageBox()
        {
            InitializeComponent();
            Caption.Text = "Closing... See you soon!";
            Message.Text = "Parser could not find combat logs. Please enable \"Write combat log to file\" in Crowfall settings, hit something and start parser again.";

            MouseDown += (sender, args) =>
            {
                if (args.ChangedButton == MouseButton.Left)
                    DragMove();
            };
        }

        private void ButtonClose_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Hyperlink_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo(SettingsControlViewModel.EnableImagesLink));
        }
    }
}