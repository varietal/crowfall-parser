﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Metrics.Extensions;
using Metrics.Models;
using OptionsModule;

namespace Metrics.Views
{
    public partial class HotkeysControl
    {
        public HotkeysControl()
        {
            InitializeComponent();
        }

        private void OnResetHotkey(object sender, RoutedEventArgs e)
        {
            var hotkeyBinding = (HotkeyBinding) ((Button) sender).DataContext;
            hotkeyBinding.Hotkey = null;
            Options.Inst.SetHotkeyBinding(hotkeyBinding.Name, null);
        }

        private void OnHotkeyChanged(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            var modifiers = Keyboard.Modifiers;
            var key = e.Key;
            Hotkey hotkey;
            switch (key)
            {
                case Key.None:
                    return;
                case Key.System:
                    key = e.SystemKey;
                    break;
            }

            if (key.IsEither(Key.Delete, Key.Back, Key.Escape) && modifiers == ModifierKeys.None)
            {
                hotkey = null;
            }
            else
            {
                if (key.IsEither(Key.LeftCtrl, Key.RightCtrl, Key.LeftAlt, Key.RightAlt, Key.LeftShift, Key.RightShift, Key.LWin, Key.RWin, Key.Clear, Key.OemClear, Key.Apps))
                    return;

                hotkey = new Hotkey(key, modifiers);
            }

            var textBox = (TextBox) sender;
            var hotkeyBinding = (HotkeyBinding) textBox.DataContext;
            hotkeyBinding.Hotkey = hotkey;
            Options.Inst.SetHotkeyBinding(hotkeyBinding.Name, hotkey);
        }
    }
}