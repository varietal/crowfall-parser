﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using OptionsModule;

namespace Metrics.Views
{
    public partial class RaidOpponentsWindow
    {
        public RaidOpponentsWindow()
        {
            InitializeComponent();
            
            SetBinding(TopProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.TopOpponents)), Delay = 3000, Mode = BindingMode.TwoWay});
            SetBinding(LeftProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.LeftOpponents)), Delay = 3000, Mode = BindingMode.TwoWay});
        }
        
        private void Opponents_OnSorting(object sender, DataGridSortingEventArgs e)
        {
            Options.Inst.RaidOpponentsSortingProperty = e.Column.SortMemberPath;
            Options.Inst.RaidOpponentsSortingDirection = e.Column.SortDirection.ToString();
        }

        public override RaidViewMode ViewMode => RaidViewMode.Opponents;
    }
}