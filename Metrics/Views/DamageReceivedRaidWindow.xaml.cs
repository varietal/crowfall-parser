﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using OptionsModule;

namespace Metrics.Views
{
    public partial class DamageReceivedRaidWindow
    {
        public DamageReceivedRaidWindow()
        {
            InitializeComponent();
            
            SetBinding(TopProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.TopDamageReceived)), Delay = 3000, Mode = BindingMode.TwoWay});
            SetBinding(LeftProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.LeftDamageReceived)), Delay = 3000, Mode = BindingMode.TwoWay});
        }
        
        private void DamageReceived_OnSorting(object sender, DataGridSortingEventArgs e)
        {
            Options.Inst.RaidDamageReceivedSortingProperty = e.Column.SortMemberPath;
            Options.Inst.RaidDamageReceivedSortingDirection = e.Column.SortDirection.ToString();
        }

        public override RaidViewMode ViewMode => RaidViewMode.DamageReceived;
    }
}