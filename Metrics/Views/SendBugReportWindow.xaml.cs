﻿using System.Windows;
using System.Windows.Input;

namespace Metrics.Views
{
    public partial class SendBugReportWindow
    {
        public SendBugReportWindow()
        {
            InitializeComponent();
            
            MouseDown += (sender, args) =>
            {
                if (args.ChangedButton == MouseButton.Left)
                    DragMove();
            };
        }

        private void ButtonClose_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}