﻿using System.Windows.Controls;
using OptionsModule;

namespace Metrics.Views
{
    /// <summary>
    /// Interaction logic for Metrics.xaml
    /// </summary>
    public partial class MainControl
    {
        public MainControl()
        {
            InitializeComponent();
        }

        private void Opponents_OnSorting(object sender, DataGridSortingEventArgs e)
        {
            Options.Inst.OpponentsSortingProperty = e.Column.SortMemberPath;
            Options.Inst.OpponentsSortingDirection = e.Column.SortDirection.ToString();
        }
        
    }
}