using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using Metrics.Models;
using NHotkey;
using NHotkey.Wpf;
using NLog;
using OptionsModule;

namespace Metrics.Extensions
{
    public static class Extensions
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static int CalcPerSecond(this int totalDamage, double duration)
        {
            return (int) (duration.Equal(0) ? 0 : totalDamage / duration * 1000);
        }

        public static bool Equal(this double left, double right)
        {
            return Math.Abs(left - right) < 1e-10;
        }

        public static void SerializeCollection<T>(this BinaryWriter writer, ICollection<T> collection) where T : INetworkObject
        {
            writer.Write(collection.Count);
            foreach (var x in collection)
                x.Serialize(writer);
        }

        public static void WriteDateTime(this BinaryWriter writer, DateTime? dateTime)
        {
            writer.Write(dateTime.HasValue ? dateTime.Value.ToString("O") : string.Empty);
        }

        public static List<T> DeserializeCollection<T>(this BinaryReader reader) where T : INetworkObject, new()
        {
            var collection = new List<T>();
            var count = reader.ReadInt32();

            for (var i = 0; i < count; i++)
            {
                collection.Add(reader.Deserialize<T>());
            }

            return collection;
        }

        public static T Deserialize<T>(this BinaryReader reader) where T : INetworkObject, new()
        {
            var result = new T();
            result.Deserialize(reader);

            return result;
        }

        public static DateTime? ReadDateTime(this BinaryReader reader)
        {
            var isoString = reader.ReadString();
            if (string.IsNullOrEmpty(isoString))
                return null;

            return DateTime.Parse(isoString);
        }

        public static void AddOrUpdateSync<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dictionary, TKey key, Func<TKey, TValue> addValueFactory,
            Func<TKey, TValue, TValue> updateValueFactory)
        {
            while (dictionary.AddOrUpdate(key, addValueFactory, updateValueFactory) == null)
            {
            }
        }

        public static TValue GetOrAddSync<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            TValue result;
            do
            {
                result = dictionary.GetOrAdd(key, value);
            } while (result == null);

            return result;
        }

        public static void RemoveSync<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dictionary, TKey key)
        {
            if (!dictionary.TryRemove(key, out _) && dictionary.ContainsKey(key))
            {
                RemoveSync(dictionary, key);
            }
        }

        public static double Bottom(this Window window)
        {
            return window.Top + window.Height;
        }

        public static double Right(this Window window)
        {
            return window.Left + window.Width;
        }

        public static bool IsLoopback(this IPEndPoint endPoint)
        {
            return IPAddress.IsLoopback(endPoint.Address.MapToIPv4());
        }

        public static string Print<T>(this IEnumerable<T> observable)
        {
            return string.Join(",", observable);
        }

        public static string Name(this ICollection<Opponent> opponents)
        {
            var name = string.Empty;
            foreach (var opponent in opponents.OrderByDescending(x => x.DamageDone + x.DamageReceived))
            {
                if ((name + opponent.Name).Length > 30)
                {
                    name += "...";
                    break;
                }

                if (name == string.Empty)
                    name = opponent.Name;
                else
                    name += ", " + opponent.Name;
            }

            return name;
        }

        public static void Bind(this string name, EventHandler<HotkeyEventArgs> handler)
        {
            var hotkeyBinding = Options.Inst.GetHotkeyBinding(name);

            try
            {
                if (hotkeyBinding.Hotkey == null)
                    HotkeyManager.Current.Remove(name);
                else
                    HotkeyManager.Current.AddOrReplace(name, hotkeyBinding.Hotkey.Key, hotkeyBinding.Hotkey.ModifierKeys, handler);
            }
            catch (Exception exception)
            {
                Logger.Error(exception, $"FAILED TO BIND {hotkeyBinding.Hotkey} for {name}");
            }
        }

        public static bool IsEither<T>(this T obj, params T[] objs)
        {
            return obj.IsEither((IEnumerable<T>) objs);
        }

        private static bool IsEither<T>(this T obj, IEnumerable<T> enumerable, IEqualityComparer<T> comparer)
        {
            if (enumerable == null)
                throw new ArgumentNullException(nameof(enumerable));
            if (comparer == null)
                throw new ArgumentNullException(nameof(comparer));

            return enumerable.Any(other => comparer.Equals(obj, other));
        }

        private static bool IsEither<T>(this T obj, IEnumerable<T> enumerable)
        {
            return obj.IsEither(enumerable, EqualityComparer<T>.Default);
        }
    }
}