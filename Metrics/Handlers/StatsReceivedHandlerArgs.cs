using System.Collections.Generic;
using Metrics.Models;

namespace Metrics.Handlers
{
    public delegate void StatsReceivedHandler(StatsReceivedHandlerArgs args);
    public class StatsReceivedHandlerArgs
    {
        public ICollection<RaidCharacter> Characters { get; set; }
        public ICollection<Opponent> Opponents { get; set; }
        public bool FromHistory { get; set; }
    }
}