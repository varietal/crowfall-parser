using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Metrics.Controls;
using Metrics.Extensions;
using Metrics.Models;
using Metrics.Services;
using Metrics.Views;
using OptionsModule;
using Prism.Commands;
using Prism.Mvvm;

namespace Metrics.ViewModels
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class MainControlViewModel : BindableBase
    {
        private readonly DispatcherTimer _characterSelector = new DispatcherTimer {Interval = TimeSpan.FromSeconds(10)};
        private readonly FightService _fightService;
        private readonly AfkNotifierService _afkNotifierService;
        private readonly List<CombatLog> _logs = new List<CombatLog>();
        private readonly DispatcherTimer _parser = new DispatcherTimer {Interval = TimeSpan.FromSeconds(1)};
        private readonly DispatcherTimer _watcher = new DispatcherTimer {Interval = TimeSpan.FromSeconds(5)};
        private readonly DispatcherTimer _simulator = new DispatcherTimer {Interval = TimeSpan.FromMilliseconds(500)};
        private DirectoryInfo _directoryInfo;
        private Fight _fight;
        private int _fightsCount;
        private bool _notifierEnabled;
        private Visibility _showDetails = Visibility.Collapsed;
        private Visibility _showRaid = Visibility.Collapsed;
        private Visibility _showOpponents = Visibility.Collapsed;
        private Visibility _showSettings = Visibility.Collapsed;
        private Visibility _showIncoming = Visibility.Collapsed;
        private StreamWriter _fightSimulationStreamWriter;

        public Fight Fight
        {
            get => _fight;
            private set
            {
                SetProperty(ref _fight, value);
                RaisePropertyChanged(nameof(Duration));
                RaisePropertyChanged(nameof(CriticalRate));
            }
        }

        public int FightsCount
        {
            get => _fightsCount;
            set => SetProperty(ref _fightsCount, value);
        }

        public Visibility ShowSettings
        {
            get => _showSettings;
            private set => SetProperty(ref _showSettings, value);
        }

        public Visibility ShowIncoming
        {
            get => _showIncoming;
            private set => SetProperty(ref _showIncoming, value);
        }

        public Visibility ShowDetails
        {
            get => _showDetails;
            private set => SetProperty(ref _showDetails, value);
        }

        public Visibility ShowRaid
        {
            get => _showRaid;
            private set => SetProperty(ref _showRaid, value);
        }

        public Visibility ShowOpponents
        {
            get => _showOpponents;
            private set => SetProperty(ref _showOpponents, value);
        }

        public bool NotifierEnabled
        {
            get => _notifierEnabled;
            private set => SetProperty(ref _notifierEnabled, value);
        }

        public string CriticalRate => $"{Fight?.CriticalRate:0}%";
        public string IncomingCriticalRate => $"{Fight?.IncomingCriticalRate:0}%";
        public string Duration => $"{Fight?.Duration / 1000:0.#}s";

        public SkillListModel DamageSkillsListModel { get; set; }

        public SkillListModel HealSkillsListModel { get; set; }
        public SkillListModel ReceivedDamageSkillsListModel { get; set; }

        public SkillListModel ReceivedHealSkillsListModel { get; set; }

        public DelegateCommand ShowSettingsCommand { get; }
        public DelegateCommand ShowIncomingCommand { get; }
        public DelegateCommand ShowDetailsCommand { get; }
        public DelegateCommand ShowRaidCommand { get; }
        public DelegateCommand SwitchNotifierCommand { get; }

        public DelegateCommand PreviousFightCommand { get; }

        public DelegateCommand NextFightCommand { get; }

        public DelegateCommand LastFightCommand { get; }

        public DelegateCommand FirstFightCommand { get; }

        public DelegateCommand ShowOpponentsCommand { get; }

        public DelegateCommand SimulateFightCommand { get; }

        public MainControlViewModel(FightService fightService, AfkNotifierService afkNotifierService, NotificationService notificationService)
        {
            _fightService = fightService;
            _afkNotifierService = afkNotifierService;

            DamageSkillsListModel = new SkillListModel("🡩 Damage", "🡩 DPS", StatType.Damage);
            HealSkillsListModel = new SkillListModel("🡩 Healing", "🡩 HPS", StatType.Heal);
            ReceivedDamageSkillsListModel = new SkillListModel("🡫 Damage", "🡫 DPS", StatType.RecievedDamage);
            ReceivedHealSkillsListModel = new SkillListModel("🡫 Healing", "🡫 HPS", StatType.Heal);

            _parser.Tick += ParserOnTick;

            _watcher.Tick += AttachToProcess;
            _watcher.Start();

            _simulator.Tick += SimulatorOnTick;

            _characterSelector.Tick += _fightService.SelectCharacter;
            _characterSelector.Start();

            _afkNotifierService.StartListeningForWindowChanges();

            ShowSettingsCommand = new DelegateCommand(OnShowSettings);
            ShowIncomingCommand = new DelegateCommand(OnShowIncoming);
            ShowDetailsCommand = new DelegateCommand(OnToggleDetails);
            ShowRaidCommand = new DelegateCommand(OnToggleRaid);
            SwitchNotifierCommand = new DelegateCommand(OnToggleNotifier);
            ShowOpponentsCommand = new DelegateCommand(OnShowOpponents);
            SimulateFightCommand = new DelegateCommand(OnSimulateFight);

            FirstFightCommand = new DelegateCommand(() =>
            {
                _fightService.First();
                UpdateFight();
            });
            LastFightCommand = new DelegateCommand(() =>
            {
                _fightService.Last();
                UpdateFight();
            });
            PreviousFightCommand = new DelegateCommand(() =>
            {
                _fightService.Previous();
                UpdateFight();
            });
            NextFightCommand = new DelegateCommand(() =>
            {
                _fightService.Next();
                UpdateFight();
            });

            Options.Inst.PropertyChanged += (sender, args) =>
            {
                switch (args.PropertyName)
                {
                    case nameof(Options.Inst.IdleDuration):
                        OnRecalculate();
                        break;
                    case Hotkeys.AfkNotifier:
                        Hotkeys.AfkNotifier.Bind((_, __) => OnToggleNotifier(notificationService));
                        break;
                    case Hotkeys.Windows:
                        Hotkeys.Windows.Bind((_, __) => OnToggleWindows());
                        break;
                    case Hotkeys.More:
                        Hotkeys.More.Bind((_, __) => OnToggleDetails());
                        break;
                    case Hotkeys.Raid:
                        Hotkeys.Raid.Bind((_, __) => OnToggleRaid());
                        break;
                }
            };

            Hotkeys.AfkNotifier.Bind((_, __) => OnToggleNotifier(notificationService));
            Hotkeys.Windows.Bind((_, __) => OnToggleWindows());
            Hotkeys.More.Bind((_, __) => OnToggleDetails());
            Hotkeys.Raid.Bind((_, __) => OnToggleRaid());
        }

        private void OnToggleNotifier(NotificationService notificationService)
        {
            OnToggleNotifier();
            notificationService.ShowSuccess(NotifierEnabled ? "AFK Notifier enabled" : "AFK Notifier disabled");
        }

        private static void OnToggleWindows()
        {
            var minimized = Application.Current.MainWindow?.Visibility == Visibility.Collapsed;

            foreach (var window in Application.Current.Windows.OfType<Window>())
            {
                window.Visibility = minimized ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private void SimulatorOnTick(object sender, EventArgs e)
        {
            var random = new Random();
            _fightSimulationStreamWriter.WriteLine($"{DateTime.UtcNow:O} INFO    COMBAT    - Combat _||_ Event=[Your Damage{random.Next(1, 3)} hit Whatever for {random.Next(1, 100)}  damage.] ");
            _fightSimulationStreamWriter.WriteLine($"{DateTime.UtcNow:O} INFO    COMBAT    - Combat _||_ Event=[Your Heal{random.Next(1, 3)} healed You for {random.Next(1, 100)} hit points.] ");
            _fightSimulationStreamWriter.WriteLine($"{DateTime.UtcNow:O} INFO    COMBAT    - Combat _||_ Event=[Hellcat{random.Next(1, 3)} Mauler Stab hit You for {random.Next(1, 100)} damage.] ");
            _fightSimulationStreamWriter.Flush();
        }

        private void OnSimulateFight()
        {
            if (_simulator.IsEnabled)
                _simulator.Stop();
            else
            {
                _fightSimulationStreamWriter ??= new StreamWriter(_logs[0].FileInfo.FullName, true);
                _simulator.Start();
            }
        }

        private void OnShowOpponents()
        {
            if (ShowOpponents == Visibility.Visible)
                ShowOpponents = Visibility.Collapsed;
            else
            {
                ShowOpponents = Visibility.Visible;
                FightService.Repaint.OnNext(PaintMode.OnTick);
            }
        }

        private void OnToggleNotifier()
        {
            NotifierEnabled = !NotifierEnabled;
            _afkNotifierService.SetNotifyState(NotifierEnabled);
        }

        private void OnToggleDetails()
        {
            if (ShowDetails == Visibility.Visible)
                ShowDetails = Visibility.Collapsed;
            else
            {
                ShowDetails = Visibility.Visible;
                FightService.Repaint.OnNext(PaintMode.OnTick);
            }
        }

        private void OnToggleRaid()
        {
            ShowRaid = ShowRaid == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        private void OnShowSettings()
        {
            ShowSettings = ShowSettings == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        private void OnShowIncoming()
        {
            ShowIncoming = ShowIncoming == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        private void OnRecalculate()
        {
            _fightService.Reset();
            foreach (var log in _logs)
            {
                log.Reader.BaseStream.Position = 0;
                ReadLog(log, true);
            }

            UpdateFight();
        }

        private void ParserOnTick(object sender, EventArgs e)
        {
            UpdateLogsList();

            foreach (var log in _logs)
            {
                ReadLog(log, false);
            }

            UpdateFight();
            RaisePropertyChanged(nameof(Fight));
            RaisePropertyChanged(nameof(CriticalRate));
            RaisePropertyChanged(nameof(IncomingCriticalRate));
            RaisePropertyChanged(nameof(Duration));
            FightService.Repaint.OnNext(PaintMode.OnTick);
        }

        private void ReadLog(CombatLog log, bool recalculate)
        {
            while (!log.Reader.EndOfStream)
            {
                var line = log.Reader.ReadLine();
                if (line != null)
                {
                    _fightService.ParseLine(log, line, false, recalculate);
                }
            }
        }

        private void UpdateFight()
        {
            if (_fightService.CurrentFight == null || Fight == _fightService.CurrentFight)
                return;

            DamageSkillsListModel.Skills = _fightService.CurrentFight.DamageDoneRecords;
            HealSkillsListModel.Skills = _fightService.CurrentFight.HealingDoneRecords;
            ReceivedDamageSkillsListModel.Skills = _fightService.CurrentFight.DamageReceivedRecords;
            ReceivedHealSkillsListModel.Skills = _fightService.CurrentFight.HealingReceivedRecords;
            FightsCount = _fightService.FightsCount;
            Fight = _fightService.CurrentFight;
            Fight.EnsureComparisons();
            FightService.Repaint.OnNext(PaintMode.OnTick);
        }

        private void UpdateLogsList()
        {
            var anHourAgo = DateTime.Now.Subtract(TimeSpan.FromHours(1));
            foreach (var fileInfo in _directoryInfo.GetFiles().Where(x => x.LastWriteTime > anHourAgo))
            {
                if (_logs.All(x => x.FileInfo.FullName != fileInfo.FullName))
                    _logs.Add(new CombatLog(fileInfo));
            }
        }

        private void AttachToProcess(object sender, EventArgs e)
        {
            var appData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var logsDirectory = appData + @"Low\Art+Craft\Crowfall\CombatLogs";

            _directoryInfo = new DirectoryInfo(logsDirectory);
            if (!_directoryInfo.Exists)
            {
                new ExtendedMessageBox().ShowDialog();
                Application.Current.Shutdown();
                return;
            }

            foreach (var fileInfo in _directoryInfo.GetFiles().OrderByDescending(x => x.LastWriteTime).Take(5))
            {
                _logs.Add(new CombatLog(fileInfo));
            }

            if (!_logs.Any())
                return;

            _parser.Start();
            _watcher.Stop();
        }
    }
}