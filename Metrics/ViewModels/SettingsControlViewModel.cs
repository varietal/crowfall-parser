using System.Diagnostics;
using System.Windows;
using Metrics.Views;
using OptionsModule;
using Prism.Commands;
using Prism.Mvvm;

namespace Metrics.ViewModels
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class SettingsControlViewModel : BindableBase
    {
        private Visibility _showSaveError = Visibility.Collapsed;
        private bool _showHotkeys;

        public DelegateCommand ResetOptionsCommand { get; }
        public DelegateCommand ResetLayoutCommand { get; }

        public DelegateCommand DonateCommand { get; }
        public DelegateCommand EnableImagesCommand { get; }
        public DelegateCommand SendBugReportCommand { get; }
        public DelegateCommand ToggleHotkeysCommand { get; }

        public static string DonationLink => "https://www.patreon.com/amaranth2517";
        public static string EnableImagesLink => "https://imgur.com/a/JRZcbvm";

        public Visibility ShowSaveError
        {
            get => _showSaveError;
            set => SetProperty(ref _showSaveError, value);
        }

        public bool ShowHotkeys
        {
            get => _showHotkeys;
            set => SetProperty(ref _showHotkeys, value);
        }

        public SettingsControlViewModel()
        {
            SendBugReportCommand = new DelegateCommand(OnSendBugReport);
            DonateCommand = new DelegateCommand(() => Process.Start(new ProcessStartInfo(DonationLink)));
            EnableImagesCommand = new DelegateCommand(() => Process.Start(new ProcessStartInfo(EnableImagesLink)));
            ResetOptionsCommand = new DelegateCommand(() => Options.Inst.ResetToDefaults());
            ResetLayoutCommand = new DelegateCommand(() => Options.Inst.ResetLayout());
            ToggleHotkeysCommand = new DelegateCommand(OnShowHotkeys);
            Options.Inst.SaveError += (sender, args) => ShowSaveError = Visibility.Visible;
        }

        private void OnShowHotkeys()
        {
            ShowHotkeys = !ShowHotkeys;
        }

        private static void OnSendBugReport()
        {
            new SendBugReportWindow().ShowDialog();
        }
    }
}