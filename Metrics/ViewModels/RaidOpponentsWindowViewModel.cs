using System;
using System.Linq;
using System.Reactive.Linq;
using Metrics.Handlers;
using Metrics.Models;
using Metrics.Services;
using OptionsModule;
using Prism.Mvvm;

namespace Metrics.ViewModels
{
    public class RaidOpponentsWindowViewModel : BindableBase
    {
        public SortableObservableCollection<Opponent> Opponents { get; } = new SortableObservableCollection<Opponent>();
        public bool FromHistory { get; set; }

        public RaidOpponentsWindowViewModel(ClientService clientService)
        {
            EnsureComparison();

            // todo: memory leak because every raid join we create new window with new viewModel
            Options.Inst.PropertyChanged += (sender, args) =>
            {
                switch (args.PropertyName)
                {
                    case nameof(Options.Inst.RaidOpponentsSortingDirection):
                    case nameof(Options.Inst.RaidOpponentsSortingProperty):
                        EnsureComparison();
                        break;
                }
            };

            Observable.FromEvent<StatsReceivedHandler, StatsReceivedHandlerArgs>(h1 => clientService.StatsReceived += h1, h2 => clientService.StatsReceived -= h2)
                .ObserveOnDispatcher().Subscribe(e =>
                {
                    FromHistory = e.FromHistory;
                    if (e.FromHistory || Opponents.Any(x => e.Opponents.All(y => y.Name != x.Name)))
                        Opponents.Clear();

                    foreach (var opponent in e.Opponents)
                    {
                        if (opponent.DamageDone <= 0 && opponent.DamageReceived <= 0)
                            continue;

                        var ch = Opponents.FirstOrDefault(x => x.Name == opponent.Name);
                        if (ch == null)
                        {
                            Opponents.Add(opponent);
                            RaisePropertyChanged(nameof(Opponents));
                        }
                        else
                        {
                            ch.DamageDone = opponent.DamageDone;
                            ch.DamageReceived = opponent.DamageReceived;
                            Opponents.Reposition(ch);
                        }
                    }
                });

            Observable.FromEvent<RaidStartedHandler, RaidStartedHandlerArgs>(h1 => clientService.RaidStarted += h1, h2 => clientService.RaidStarted -= h2)
                .ObserveOnDispatcher().Subscribe(e =>
                {
                    if (!FromHistory)
                        Opponents.Clear();
                });
        }


        private void EnsureComparison()
        {
            var sign = Options.GetCompareSign(Options.Inst.RaidOpponentsSortingDirection);
            var property = Options.Inst.RaidOpponentsSortingProperty;
            Comparison<Opponent> comparison = property switch
            {
                "DamageReceived" => (x, y) => sign * y.DamageReceived.CompareTo(x.DamageReceived),
                "DamageDone" => (x, y) => sign * y.DamageDone.CompareTo(x.DamageDone),
                _ => (x, y) => 0
            };

            Opponents.EnsureComparison(comparison);
        }
    }
}