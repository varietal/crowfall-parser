using System.Diagnostics;
using Prism.Commands;
using Prism.Mvvm;

namespace Metrics.ViewModels
{
    public class PatreonWindowViewModel : BindableBase
    {
        private string _donateLabel = "Become a patron!";
        private string _closeLabel = "Not today";
        public DelegateCommand DonateCommand { get; }

        public string DonateLabel
        {
            get => _donateLabel;
            private set => SetProperty(ref _donateLabel, value);
        }

        public string CloseLabel
        {
            get => _closeLabel;
            private set => SetProperty(ref _closeLabel, value);
        }

        public PatreonWindowViewModel()
        {
            DonateCommand = new DelegateCommand(() => Process.Start(new ProcessStartInfo(SettingsControlViewModel.DonationLink)));
        }
    }
}