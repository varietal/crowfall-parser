using Metrics.Models;
using Metrics.Services;
using OptionsModule;

namespace Metrics.ViewModels
{
    public class DamageDoneRaidWindowViewModel : RaidWindowViewModel
    {
        protected override string GetProperty()
        {
            return Options.Inst.RaidDamageDoneSortingProperty;
        }

        protected override string GetDirection()
        {
            return Options.Inst.RaidDamageDoneSortingDirection;
        }

        protected override bool AddPlayer(RaidCharacter character)
        {
            return character.Fight.DamageDone?.Total > 0;
        }
        
        protected override void UpdatePlayer(RaidPlayer ch, RaidCharacter character)
        {
            ch.Total = character.Fight.DamageDone.Total;
            ch.PerSecond = character.Fight.DamageDone.PerSecond;
        }

        protected override  RaidPlayer NewPlayer(RaidCharacter character)
        {
            return new RaidPlayer(character.Name, character.Fight.DamageDone.Total, character.Fight.DamageDone.PerSecond);
        }

        public DamageDoneRaidWindowViewModel(ClientService clientService) : base(clientService)
        {
            Options.Inst.PropertyChanged += (sender, args) =>
            {
                switch (args.PropertyName)
                {
                    case nameof(Options.Inst.RaidDamageDoneSortingDirection):
                    case nameof(Options.Inst.RaidDamageDoneSortingProperty):
                        EnsureComparison();
                        break;
                }
            };
        }
    }
}