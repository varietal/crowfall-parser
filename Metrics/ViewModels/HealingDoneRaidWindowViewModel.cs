using Metrics.Models;
using Metrics.Services;
using OptionsModule;

namespace Metrics.ViewModels
{
    public class HealingDoneRaidWindowViewModel : RaidWindowViewModel
    {
        public HealingDoneRaidWindowViewModel(ClientService clientService) : base(clientService)
        {
            Options.Inst.PropertyChanged += (sender, args) =>
            {
                switch (args.PropertyName)
                {
                    case nameof(Options.Inst.RaidHealingDoneSortingDirection):
                    case nameof(Options.Inst.RaidHealingDoneSortingProperty):
                        EnsureComparison();
                        break;
                }
            };
        }
        
        protected override void UpdatePlayer(RaidPlayer ch, RaidCharacter character)
        {
            ch.Total = character.Fight.HealingDone.Total;
            ch.PerSecond = character.Fight.HealingDone.PerSecond;
        }

        protected override  RaidPlayer NewPlayer(RaidCharacter character)
        {
            return new RaidPlayer(character.Name, character.Fight.HealingDone.Total, character.Fight.HealingDone.PerSecond);
        }
        
        protected override string GetProperty()
        {
            return Options.Inst.RaidHealingDoneSortingProperty;
        }

        protected override string GetDirection()
        {
            return Options.Inst.RaidHealingDoneSortingDirection;
        }

        protected override bool AddPlayer(RaidCharacter character)
        {
            return character.Fight.HealingDone?.Total > 0;
        }
    }
}