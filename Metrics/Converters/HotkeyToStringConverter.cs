using System;
using System.Globalization;
using System.Windows.Data;
using OptionsModule;

namespace Metrics.Converters
{
    public class HotkeyToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((Hotkey) value)?.ToString().Replace("None+", "");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}