using System;
using System.Globalization;
using System.Windows.Data;
using OptionsModule;

namespace Metrics.Converters
{
    public class SortingToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // todo: hardcoded string
            return value is string property && property == "DamageDone" ? Options.Inst.DamageDoneColor : Options.Inst.DamageReceivedColor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}