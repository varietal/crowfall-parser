using System;
using System.Globalization;
using System.Windows.Data;

namespace Metrics.Converters
{
    public class RaidButtonLabelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null && (bool) value ? "Leave Raid" : "Join Raid";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}