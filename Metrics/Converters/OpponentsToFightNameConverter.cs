using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using Metrics.Extensions;
using Metrics.Models;

namespace Metrics.Converters
{
    public class OpponentsToFightNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is ICollection<Opponent> collection))
                return string.Empty;

            return collection.Name();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}