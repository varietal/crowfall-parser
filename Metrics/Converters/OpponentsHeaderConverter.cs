using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Metrics.Models;

namespace Metrics.Converters
{
    public class OpponentsHeaderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is SortableObservableCollection<Opponent> opponents ? $"{opponents.Count(x => !x.SelfDamage)} Opponents" : "Opponents";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}