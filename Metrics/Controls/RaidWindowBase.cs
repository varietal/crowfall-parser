using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Metrics.Extensions;
using OptionsModule;

namespace Metrics.Controls
{
    public abstract class RaidWindowBase : Window
    {
        public abstract RaidViewMode ViewMode { get; }

        protected RaidWindowBase()
        {
            MouseDown += (sender, args) =>
            {
                if (args.ChangedButton != MouseButton.Left)
                    return;

                DragMove();
                Magnet();
            };

            Options.Inst.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == nameof(Options.Inst.Topmost) && Options.Inst.Topmost)
                {
                    Dispatcher?.InvokeAsync(() =>
                    {
                        Thread.Sleep(500);
                        Activate();
                    });
                }
            };
        }

        protected void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            Options.Inst.SetRaidVisibility(ViewMode, false);
        }

        protected void FirstHeader_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                DragMove();
                Magnet();
            }

            e.Handled = true;
        }

        private void Magnet()
        {
            var windows = Application.Current.Windows.OfType<Window>().Where(x => x != this);
            foreach (var window in windows)
            {
                if (Connects(Top, 0))
                    Top = 0;

                if (Connects(this.Bottom(), SystemParameters.PrimaryScreenHeight))
                    Top = SystemParameters.PrimaryScreenHeight - Height;

                if (Connects(Top, window.Bottom()))
                    Top = window.Bottom();

                if (Connects(Top, window.Top))
                    Top = window.Top;

                if (Connects(this.Bottom(), window.Top))
                    Top = window.Top - Height;

                if (Connects(Left, 0))
                    Left = 0;

                if (Connects(this.Right(), SystemParameters.PrimaryScreenWidth))
                    Left = SystemParameters.PrimaryScreenWidth - Width;

                if (Connects(Left, window.Right()))
                    Left = window.Right();

                if (Connects(Left, window.Left))
                    Left = window.Left;

                if (Connects(this.Right(), window.Left))
                    Left = window.Left - Width;
            }
        }

        private static bool Connects(double x, double y)
        {
            return Math.Abs(x - y) < Options.Inst.MagnetDistance;
        }
    }
}