using System.IO;

namespace Metrics.Commands
{
    public class ReconnectCommand : Command
    {
        protected override Commands Id => Commands.Reconnect;
        public override bool UseRelay => true;
        public override bool AllowRelay => true;

        public override void Serialize(BinaryWriter writer)
        {
        }

        public override void Deserialize(BinaryReader reader)
        {
        }
    }
}