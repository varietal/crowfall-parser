using System.Collections.Generic;
using System.IO;
using System.Linq;
using Metrics.Extensions;
using Metrics.Models;

namespace Metrics.Commands
{
    public class RaidStatsCommand : Command
    {
        protected override Commands Id => Commands.RaidStats;
        public override bool UseRelay => false;
        public override bool AllowRelay => true;
        public ICollection<RaidCharacter> Characters { get; private set; }
        public ICollection<Opponent> Opponents { get; private set; }

        public RaidStatsCommand(ICollection<RaidCharacter> characters)
        {
            Characters = characters;
            Opponents = GetOpponents(characters);
        }

        private static List<Opponent> GetOpponents(ICollection<RaidCharacter> characters)
        {
            var opponents = new List<Opponent>();

            foreach (var character in characters)
            {
                foreach (var characterOpponent in character.Fight.Opponents)
                {
                    var opponent = opponents.FirstOrDefault(x => x.Name == characterOpponent.Name);
                    if (opponent == null)
                        opponents.Add(new Opponent(characterOpponent.Name, characterOpponent.DamageDone, characterOpponent.DamageReceived, characterOpponent.DamageDoneAbsorbed, characterOpponent.DamageReceivedAbsorbed));
                    else
                    { 
                        opponent.DamageReceived += characterOpponent.DamageReceived;
                        opponent.DamageDone += characterOpponent.DamageDone;
                    }
                }
            }

            return opponents;
        }

        public RaidStatsCommand()
        {
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.SerializeCollection(Characters);
            writer.SerializeCollection(Opponents);
        }

        public override void Deserialize(BinaryReader reader)
        {
            Characters = reader.DeserializeCollection<RaidCharacter>();
            Opponents = reader.DeserializeCollection<Opponent>();
        }
    }
}