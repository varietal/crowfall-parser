using System;
using System.IO;
using Metrics.Extensions;

namespace Metrics.Commands
{
    public class RaidCommand : Command
    {
        protected override Commands Id => Commands.Raid;
        public override bool UseRelay => true;
        public override bool AllowRelay => true;

        public DateTime? Started { get; private set; }

        public DateTime? ServerTime { get; set; }

        public RaidCommand(DateTime started)
        {
            Started = started;
            ServerTime = DateTime.Now;
        }

        public RaidCommand()
        {
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.WriteDateTime(Started);
            writer.WriteDateTime(ServerTime);
        }

        public override void Deserialize(BinaryReader reader)
        {
            Started = reader.ReadDateTime();
            ServerTime = reader.ReadDateTime();
        }
        
        public override string ToString()
        {
            return base.ToString() + $"[{Started:O}][{ServerTime:O}]";
        }
    }
}