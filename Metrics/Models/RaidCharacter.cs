using System.IO;
using Metrics.Extensions;
using Prism.Mvvm;

namespace Metrics.Models
{
    public class RaidCharacter : BindableBase, IRecord, INetworkObject
    {
        private int _order;
        public string Name { get; set; }

        public RaidFight Fight { get; set; }

        // todo: think
        public int Order
        {
            get => _order;
            set => SetProperty(ref _order, value);
        }

        public int Percent { get; set; }

        public void Serialize(BinaryWriter writer)
        {
            writer.Write(Name);
            Fight.Serialize(writer);
        }

        public void Deserialize(BinaryReader reader)
        {
            Name = reader.ReadString();
            Fight = reader.Deserialize<RaidFight>();
        }
    }
}