using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Metrics.Models
{
    public class SortableObservableCollection<T> : ObservableCollection<T> where T : IRecord
    {
        private Comparison<T> _comparison = (x, y) => 0;

        public new void Add(T item)
        {
            var index = GetIndex(Items, item, 0, Count - 1);
            Insert(index, item);

            item.Order = index + 1;
            ReorderNextItems(index);
        }

        private void ReorderNextItems(int index)
        {
            for (int i = index + 1; i < Count; i++)
            {
                var item = base[i];
                item.Order++;
            }
        }

        private int GetIndex(IList<T> collection, T item, int low, int high)
        {
            if (high < low)
                return low;

            var mid = low + (high - low) / 2;

            var result = _comparison.Invoke(collection[mid], item);
            if (result < 0)
                return GetIndex(collection, item, mid + 1, high);

            if (result > 0)
                return GetIndex(collection, item, low, mid - 1);

            return mid;
        }

        public void EnsureComparison(Comparison<T> comparison)
        {
            _comparison = comparison;
            Sort();
        }

        private void Sort()
        {
            var list = new List<T>();
            list.AddRange(this);
            this.Clear();

            foreach (var item in list)
                this.Add(item);
        }

        public void Reposition(T item)
        {
            var oldIndex = IndexOf(item);
            var list = this.ToList();
            list.Remove(item);

            var newIndex = GetIndex(list, item, 0, list.Count - 1);
            if (oldIndex != newIndex && newIndex < Count)
            {
                Move(oldIndex, newIndex);
                item.Order = newIndex + 1;

                if (newIndex > oldIndex)
                {
                    for (var i = oldIndex; i < newIndex; i++)
                    {
                        var item2 = base[i];
                        item2.Order--;
                    }
                }
                else
                {
                    for (var i = newIndex + 1; i <= oldIndex; i++)
                    {
                        var item2 = base[i];
                        item2.Order++;
                    }
                }
            }
        }
    }
}