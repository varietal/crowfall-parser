using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Metrics.Models
{
    public class CombatLog
    {
        public CombatLog(string id, string name)
        {
            Id = id;
            Name = name;
        }

        public CombatLog(FileInfo fileInfo)
        {
            FileInfo = fileInfo;
            Reader = new StreamReader(FileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            Id = fileInfo.Name.Split('_').ElementAtOrDefault(1);

            var regexes = new List<Regex>
            {
                new Regex(@"(.*) INFO    PHYSICSOBJECTMANAGER - Marking local player object for deletion _\|\|_ PlayerName=\[(.*)\] "),
                new Regex(@"(.*) INFO    MOVEMENT  - Initialized local player _\|\|_ Name=\[(.*)\] Position=(.*)"),
                new Regex(@"(.*) INFO    MOVEMENT  - Setting movement paused based on load screen _\|\|_ Name=\[(.*)\] Position=(.*)"),
            };

            Task.Run(() =>
            {
                Name = Id;
                var clientLog = FileInfo.Directory?.Parent?.GetFiles().FirstOrDefault(x => x.FullName.Contains(Id));
                if (clientLog != null)
                {
                    var reader = new StreamReader(clientLog.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        if (line != null)
                        {
                            if (FindAccountName(regexes, line))
                                return;
                        }
                    }
                }
            });

            // todo: watch for logout/login to another account - name changes
        }

        private bool FindAccountName(List<Regex> regexes, string line)
        {
            foreach (var regex in regexes)
            {
                var match = regex.Match(line);
                if (match.Success)
                {
                    var name = match.Groups[2].Value;
                    if (!string.IsNullOrEmpty(name) && !name.Contains("@"))
                    {
                        Name = name;
                        return true;
                    }
                }
            }

            return false;
        }

        public FileInfo FileInfo { get; }
        public StreamReader Reader { get; }
        public string Id { get; }
        public string Name { get; private set; }
    }
}