using System.Collections.Generic;
using System.IO;
using Metrics.Extensions;

namespace Metrics.Models
{
    public class RaidFight : INetworkObject
    {
        // public int MaximumHit { get; set; }
        // public string MaximumHitName { get; set; }
        // public int MaximumHeal { get; set; }
        // public string MaximumHealName { get; set; }
        // public double CriticalRate { get; set; }

        public Skill DamageDone { get; set; }
        public Skill DamageReceived { get; set; }
        public Skill HealingDone { get; set; }
        public Skill HealingReceived { get; set; }
        public IEnumerable<Opponent> Opponents { get; set; }

        public void Serialize(BinaryWriter writer)
        {
            DamageDone.Serialize(writer);
            DamageReceived.Serialize(writer);
            HealingDone.Serialize(writer);
            HealingReceived.Serialize(writer);
        }

        public void Deserialize(BinaryReader reader)
        {
            DamageDone = reader.Deserialize<Skill>();
            DamageReceived = reader.Deserialize<Skill>();
            HealingDone = reader.Deserialize<Skill>();
            HealingReceived = reader.Deserialize<Skill>();
        }
    }
}