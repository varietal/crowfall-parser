using System.Collections.Generic;

namespace Metrics.Models
{
    public class Character
    {
        public CombatLog Log { get; }

        public Character(CombatLog log)
        {
            Log = log;
        }

        public List<Fight> Fights { get; } = new List<Fight>();
        public Fight CurrentFight { get; set; }
    }
}