using Prism.Mvvm;

namespace Metrics.Models
{
    public class RaidPlayer : BindableBase, IRecord
    {
        public string Name { get; }

        public int Total
        {
            get => _total;
            set => SetProperty(ref _total, value);
        }

        public int PerSecond
        {
            get => _perSecond;
            set => SetProperty(ref _perSecond, value);
        }

        private int _order;
        private int _total;
        private int _perSecond;

        public RaidPlayer(string name, int total, int perSecond)
        {
            Name = name;
            Total = total;
            PerSecond = perSecond;
        }

        public int Order
        {
            get => _order;
            set => SetProperty(ref _order, value);
        }

        public int Percent { get; set; }
    }
}