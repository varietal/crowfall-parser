namespace Metrics.Models
{
    public class Chunk
    {
        public byte Order { get; set; }
        public byte Count { get; set; }
        public ushort CommandId { get; set; }
        public byte[] Data { get; set; }
    }
}