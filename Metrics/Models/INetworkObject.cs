using System.IO;

namespace Metrics.Models
{
    public interface INetworkObject
    {
        void Serialize(BinaryWriter writer);
        void Deserialize(BinaryReader reader);
    }
}