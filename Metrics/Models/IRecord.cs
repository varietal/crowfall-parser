namespace Metrics.Models
{
    public interface IRecord
    {
        int Order { get; set; }
        int Percent { get; set; }
    }
}