﻿﻿using System.Net;

 namespace Metrics.Models
{
    public class Computer
    {
        public IPEndPoint EndPoint { get; set; }

        public bool IsServer { get; set; }


        public override string ToString()
        {
            var type = IsServer ? "SERVER" : "CLIENT";
            
            return $"{EndPoint}[{type}]";
        }
    }
}