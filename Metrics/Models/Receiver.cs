using System;
using System.Net;

namespace Metrics.Models
{
    public class Receiver
    {
        public IPAddress Address { get; set; }
        public int RelayPort { get; set; }

        public Receiver(IPEndPoint endPoint, bool byUdp)
        {
            Address = endPoint.Address.ToString() == "::ffff:0:0" ? IPAddress.Parse("::ffff:127.0.0.1") : endPoint.Address;
            if (byUdp)
                UdpPort = endPoint.Port;
            else
                RelayPort = endPoint.Port;
        }

        public int UdpPort { get; set; }

        public ushort LastCommandId { get; private set; }
        public bool ConnectedByUdp { get; set; }
        public DateTime? AliveAt { get; set; }

        public void CommandSent()
        {
            LastCommandId++;
        }

        public bool LostConnection() => !IsLoopback() && ConnectedByUdp && AliveAt != null && AliveAt.Value.AddSeconds(10) < DateTime.Now;

        public IPEndPoint ToUdpEndPoint() => new IPEndPoint(Address, UdpPort);

        public IPEndPoint ToRelayEndPoint() => new IPEndPoint(Address, RelayPort);

        public bool IsLoopback() => IPAddress.IsLoopback(Address.MapToIPv4());
    }
}