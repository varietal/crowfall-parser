﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Metrics.Commands;
using Metrics.Models;
using Metrics.Services;
using NLog;
using NLog.Config;
using NLog.Targets;
using Xunit;
using Xunit.Abstractions;

namespace Crowbars.Tests
{
    // todo: restore tests
    public class NetworkServiceTests : IDisposable
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private long _expectedPackets;
        private long _actualPackets;
        private readonly MemoryTarget _memoryTarget;

        public NetworkServiceTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            var configuration = new LoggingConfiguration();
            _memoryTarget = new MemoryTarget {Name = "mem"};

            configuration.AddTarget(_memoryTarget);
            configuration.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, _memoryTarget));
            LogManager.Configuration = configuration;
            var logger = LogManager.GetLogger("test");
            logger.Info("ow noos");
        }

        [Fact]
        public void TestNetworking()
        {
            var characters = new List<RaidCharacter>();
            for (var i = 0; i < 10; i++)
            {
                var mockCharacter = MockCharacter(i, 10, 1);
                characters.Add(new RaidCharacter() {Fight = mockCharacter.CurrentFight.ToRaidFight(), Name = mockCharacter.Log.Name});
            }

            var data = new RaidStatsCommand(characters).Serialize();
            const ushort commandId = 0;
            var parserClient = new ParserClient(null, null, null);
            var parts = ParserClient.CreateChunks(data, commandId).ToList();
            var count = data.Length / ParserClient.ChunkSize + 1;
            Assert.Equal(count, parts.Count);
            Assert.True(count < 10); // note: there should be reasonable amount of chunks, no matter how big is data

            var endpoint = new IPEndPoint(24234234, 53003);

            for (var i = 0; i < parts.Count; i++)
            {
                var part = parts[i];
                var chunk = ParserClient.ParseChunk(part);
                Assert.Equal(i, chunk.Order);
                Assert.Equal(commandId, chunk.CommandId);
                Assert.Equal(count, chunk.Count);
                // todo: test chunk.Data
                parserClient.AddChunk(endpoint, chunk);
            }

            var constructedData = parserClient.TryConstructCommand(endpoint, commandId);
            Assert.Equal(data, constructedData);
        }

        [Fact]
        public async Task TestPerformance()
        {
            // note: only use for CI testing
            return;

            const int clientsCount = 100;

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var serverService = new ServerService();
            var serverClient = new ParserClient(null, serverService, null);
            serverClient.Listen();
            _testOutputHelper.WriteLine("Start server: " + stopWatch.Elapsed.TotalSeconds.ToString(CultureInfo.InvariantCulture));

            var clients = new ParserClient[clientsCount];
            var tasks = new List<Task>();
            for (var i = 0; i < clientsCount; i++)
            {
                var clientService = new ClientService();
                var client = new ParserClient(null, clientService, null);
                clients[i] = client;
                client.Listen();

                tasks.Add(Task.Run(() => clientService.JoinServer(serverClient.EndPoint)));
            }

            await Task.WhenAll(tasks);
            await Task.Delay(1000);
            _testOutputHelper.WriteLine("Connect clients: " + stopWatch.Elapsed.TotalSeconds.ToString(CultureInfo.InvariantCulture));

            Assert.True(serverClient.IsConnected());
            Assert.All(clients, x => Assert.True(x.IsConnected()));
            Assert.Equal(clientsCount, serverService.Receivers.Count);

            Assert.InRange(serverClient.ReceivedCommands, AllowedDrops(serverClient.ReceivedCommands, clientsCount), clientsCount);
            Assert.All(clients, x => Assert.InRange(x.ReceivedCommands, AllowedDrops(x.ReceivedCommands, 1), 1));
            _testOutputHelper.WriteLine("Assert connections: " + stopWatch.Elapsed.TotalSeconds.ToString(CultureInfo.InvariantCulture));

            tasks.Clear();

            // var opponentsAsserts = new List<(List<Opponent>, int)>();
            // var random = new Random();

            const int secs = 100;
            var sec = 0;

            var timer = new Timer(async sender =>
            {
                sec++;
                if (sec > secs)
                    return;

                // Task.Run(() => opponentsAsserts.Add((serverService.LastRaidStatsCommand.Opponents.ToList(), sec - 1)));

                for (var i = 0; i < clientsCount; i++)
                {
                    // if (random.Next(0, 6000) > 5980)
                    // {
                    //     // serverService.LogCommands = true;
                    //     await clientServices[i].Stop();
                    //     clientServices[i].Listen(null, null);
                    //     Task.Run(() => clientServices[i].JoinServer(serverEndpoint));
                    // }

                    var client = clients[i];
                    await Task.Delay(5);
                    // await Task.Delay(1); note: simulate buffer overflow for packet drops
                    var character = MockCharacter(i, clientsCount, sec);
                    await client.SendCommand(new List<Receiver> {new Receiver(serverClient.EndPoint, true)}, new ClientStatsCommand(character.CurrentFight, "raid123"));
                }
            });
            timer.Change(TimeSpan.Zero, TimeSpan.FromSeconds(1));

            await Task.Delay((secs + 2) * 1000);
            // opponentsAsserts.Add((serverService.LastRaidStatsCommand.Opponents.ToList(), secs));

            _testOutputHelper.WriteLine("Send client stats: " + stopWatch.Elapsed.TotalSeconds.ToString(CultureInfo.InvariantCulture));
            _testOutputHelper.WriteLine("Raid stats sent: " + serverService.RaidStatsSent);

            Assert.True(serverClient.IsConnected());
            Assert.All(clients, x => Assert.True(x.IsConnected()));

            // foreach (var (opponents, time) in opponentsAsserts)
            // {
            //     _testOutputHelper.WriteLine($"Opponents: {time} : " + JsonConvert.SerializeObject(opponents));
            // }

            Assert.All(clients, x => Assert.InRange(x.ReceivedCommands, AllowedDrops(x.ReceivedCommands, 1 + serverService.RaidStatsSent), 1 + serverService.RaidStatsSent));
            Assert.InRange(serverClient.ReceivedCommands, AllowedDrops(serverClient.ReceivedCommands, clientsCount * (1 + secs)), clientsCount * (1 + secs));
            _testOutputHelper.WriteLine("Assert messages are received: " + stopWatch.Elapsed.TotalSeconds.ToString(CultureInfo.InvariantCulture));

            // foreach (var (opponents, time) in opponentsAsserts)
            // {
            //     foreach (var opponent in opponents)
            //     {
            //         Assert.Equal(time * 100 * clientsCount, opponent.DamageDone);
            //         Assert.Equal(time * 100 * clientsCount, opponent.DamageReceived);
            //     }
            // }

            tasks.Clear();
            serverClient.Stop();
            tasks.AddRange(clients.Select(client => Task.Run(client.Stop)));

            await Task.WhenAll(tasks);
            _testOutputHelper.WriteLine("Stop services: " + stopWatch.Elapsed.TotalSeconds.ToString(CultureInfo.InvariantCulture));

            Assert.False(serverClient.IsConnected());
            Assert.All(clients, x => Assert.False(x.IsConnected()));

            _testOutputHelper.WriteLine("Finish: " + stopWatch.Elapsed.TotalSeconds.ToString(CultureInfo.InvariantCulture));
        }

        [Fact]
        public async Task TestTimeSync()
        {
            var serverService = new ServerService();
            var serverClient = new ParserClient(null, serverService, null);
            serverClient.Listen();

            var clientService = new ClientService();
            var client = new ParserClient(null, clientService, null);
            client.Listen();

            var raidStarted = DateTime.UtcNow;
            var shift = TimeSpan.FromSeconds(30);
            await serverClient.SendCommand(new List<Receiver> {new Receiver(client.EndPoint, true)}, new RaidCommand(raidStarted) {ServerTime = DateTime.Now - shift});

            await Task.Delay(100);
            Assert.InRange(clientService.SyncTime, shift, shift + TimeSpan.FromMilliseconds(200));

            var serverStarted = raidStarted;
            // note: sending stats from client far ahead in time of server, but raidStarted should not change
            var mockCharacter = MockCharacter(0, 1, 1);
            await client.SendCommand(new List<Receiver> {new Receiver(serverClient.EndPoint, true)}, new ClientStatsCommand(mockCharacter.CurrentFight, "raid123"));
            await Task.Delay(100);
            Assert.Equal(raidStarted, serverStarted);
        }

        // [Fact]
        // public void TestRaidOpponents()
        // {
        //     
        // }

        private static Character MockCharacter(int i, int clientsCount, int secs)
        {
            var fight = new Fight
            {
                Started = DateTime.Now,
                DamageDone = new Skill {Total = 100, PerSecond = 100, Crits = 1, Hits = 1},
                HealingDone = new Skill {Total = 100, PerSecond = 100, Crits = 1, Hits = 1},
                DamageReceived = new Skill {Total = 100, PerSecond = 100, Crits = 1, Hits = 1},
                HealingReceived = new Skill {Total = 100, PerSecond = 100, Crits = 1, Hits = 1}
            };

            for (var j = 0; j < clientsCount; j++)
            {
                fight.Opponents.Add(new Opponent($"Opponent {j}", secs * 100, secs * 100, 0,0));
            }

            return new Character(new CombatLog("test", "Client " + i)) {CurrentFight = fight};
        }

        private long AllowedDrops(long actual, long expected)
        {
            _expectedPackets += expected;
            _actualPackets += actual;
            return (long) Math.Round(expected * 0.9d);
        }

        public void Dispose()
        {
            if (_expectedPackets > 0)
            {
                _testOutputHelper.WriteLine("Expected packets: " + _expectedPackets);
                _testOutputHelper.WriteLine("Actual packets: " + _actualPackets);
                _testOutputHelper.WriteLine("Drops: " + (int) (100 * (1 - _actualPackets / (double) _expectedPackets)) + "%");
            }

            var logs = _memoryTarget.Logs;
            foreach (var log in logs)
            {
                _testOutputHelper.WriteLine(log);
            }
        }
    }
}