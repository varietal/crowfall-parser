﻿using System;
using Metrics.Extensions;
using Metrics.Models;
using Metrics.Services;
using Xunit;

namespace Crowbars.Tests
{
    public class FightServiceTests
    {
        public FightServiceTests()
        {
            _fightService = new FightService(new ClientService());
            _log = new CombatLog("", "");
        }

        private readonly FightService _fightService;
        private readonly CombatLog _log;

        [Fact]
        public void TestDamageLine()
        {
            _fightService.ParseLine(_log, "2019-01-30T11:00:00.000Z INFO    COMBAT    - Combat _||_ Event=[Your Smash hit Practice Dummy for 500  damage.] ", false);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(0));
            Assert.Equal(1, _fightService.CurrentFight.DamageDoneRecords.Count);

            _fightService.ParseLine(_log, "2019-01-30T11:00:02.000Z INFO    COMBAT    - Combat _||_ Event=[Your Smash hit Practice Dummy for 1500  damage.] ", false);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:02.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(2000));
            Assert.Equal(1, _fightService.CurrentFight.DamageDoneRecords.Count);
            var total = _fightService.CurrentFight.DamageDone;
            Assert.NotNull(total);
            Assert.Equal(2000, total.Total);
            Assert.Equal(1000, total.PerSecond);
        }

        [Fact]
        public void TestDamageLineRu()
        {
            _fightService.ParseLine(_log, "2019-01-30T11:00:00.000Z INFO    COMBAT    - Combat _||_ Event=[Ваш: \"Ужасный разрез\" поражает цель Practice Dummy на 500  урон.] ", false);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(0));
            Assert.Equal(1, _fightService.CurrentFight.DamageDoneRecords.Count);

            _fightService.ParseLine(_log, "2019-01-30T11:00:02.000Z INFO    COMBAT    - Combat _||_ Event=[Ваш: \"Ужасный разрез\" поражает цель Practice Dummy на 1500  урон.] ", false);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:02.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(2000));
            Assert.Equal(1, _fightService.CurrentFight.DamageDoneRecords.Count);
            var total = _fightService.CurrentFight.DamageDone;
            Assert.NotNull(total);
            Assert.Equal(2000, total.Total);
            Assert.Equal(1000, total.PerSecond);
        }

        [Fact]
        public void TestHealLine()
        {
            _fightService.ParseLine(_log, "2019-01-30T11:00:00.000Z INFO    COMBAT    - Combat _||_ Event=[Your Ultimate Warrior healed You for 2000 hit points.] ", false);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(0));
            Assert.Equal(1, _fightService.CurrentFight.HealingDoneRecords.Count);

            _fightService.ParseLine(_log, "2019-01-30T11:00:02.000Z INFO    COMBAT    - Combat _||_ Event=[Your Ultimate Warrior healed You for 4000 hit points.] ", false);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:02.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(2000));
            Assert.Equal(1, _fightService.CurrentFight.HealingDoneRecords.Count);
            var total = _fightService.CurrentFight.HealingDone;
            Assert.NotNull(total);
            Assert.Equal(6000, total.Total);
            Assert.Equal(3000, total.PerSecond);
        }

        [Fact]
        public void TestHealLineRu()
        {
            _fightService.ParseLine(_log, "2019-01-30T11:00:00.000Z INFO    COMBAT    - Combat _||_ Event=[Ваш: \"Разворот с кинжалом\" исцеляет цель Вы на 2000 оч..] ", false);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(0));
            Assert.Equal(1, _fightService.CurrentFight.HealingDoneRecords.Count);

            _fightService.ParseLine(_log, "2019-01-30T11:00:02.000Z INFO    COMBAT    - Combat _||_ Event=[Ваш: \"Разворот с кинжалом\" исцеляет цель Вы на 4000 оч..] ", false);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:02.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(2000));
            Assert.Equal(1, _fightService.CurrentFight.HealingDoneRecords.Count);
            var total = _fightService.CurrentFight.HealingDone;
            Assert.NotNull(total);
            Assert.Equal(6000, total.Total);
            Assert.Equal(3000, total.PerSecond);
        }


        [Fact]
        public void TestReceivedLines()
        {
            _fightService.ParseLine(_log, "2019-01-30T11:00:00.000Z INFO    COMBAT    - Combat _||_ Event=[Muskhog Berserker Gore hit You for 48  damage.] ", false);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(0));
            Assert.Equal(1, _fightService.CurrentFight.DamageReceivedRecords.Count);

            _fightService.ParseLine(_log, "2019-01-30T11:00:02.000Z INFO    COMBAT    - Combat _||_ Event=[Your Ultimate Warrior healed You for 4000 hit points.] ", false);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:02.000Z"), _fightService.CurrentFight.LastAction);

            Assert.True(_fightService.CurrentFight.Duration.Equal(2000));
            Assert.Equal(1, _fightService.CurrentFight.HealingReceivedRecords.Count);
            var total = _fightService.CurrentFight.HealingReceived;
            Assert.NotNull(total);
            Assert.Equal(4000, total.Total);
            Assert.Equal(2000, total.PerSecond);
        }

        [Fact]
        public void TestReceivedLinesRu()
        {
            _fightService.ParseLine(_log, "2019-01-30T11:00:00.000Z INFO    COMBAT    - Combat _||_ Event=[Muskhog Berserker: \"Резня\" поражает цель Игрок на 48  урон.] ", false);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(0));
            Assert.Equal(1, _fightService.CurrentFight.DamageReceivedRecords.Count);

            _fightService.ParseLine(_log, "2019-01-30T11:00:02.000Z INFO    COMBAT    - Combat _||_ Event=[Ваш: \"Разворот с кинжалом\" исцеляет цель Игрок на 4000 оч..] ", false);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:02.000Z"), _fightService.CurrentFight.LastAction);

            Assert.True(_fightService.CurrentFight.Duration.Equal(2000));
            Assert.Equal(1, _fightService.CurrentFight.HealingReceivedRecords.Count);
            var total = _fightService.CurrentFight.HealingReceived;
            Assert.NotNull(total);
            Assert.Equal(4000, total.Total);
            Assert.Equal(2000, total.PerSecond);
        }

        [Fact]
        public void TestGetPreviousWord()
        {
            var tail = " Bleed damage (Critical).";
            Assert.Equal("Bleed", FightService.GetPreviousWord(tail, "damage"));
        }
    }
}