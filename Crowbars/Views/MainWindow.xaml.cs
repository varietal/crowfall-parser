﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using Metrics.ViewModels;
using Metrics.Views;
using NLog;
using OptionsModule;
using Application = System.Windows.Application;
using Binding = System.Windows.Data.Binding;
using ContextMenu = System.Windows.Forms.ContextMenu;
using MenuItem = System.Windows.Forms.MenuItem;

namespace Crowbars.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            MouseDown += (sender, args) =>
            {
                if (args.ChangedButton == MouseButton.Left)
                    DragMove();
            };

            SetBinding(TopProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.Top)), Delay = 3000, Mode = BindingMode.TwoWay});
            SetBinding(LeftProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.Left)), Delay = 3000, Mode = BindingMode.TwoWay});

            Closed += (sender, args) =>
            {
                LogManager.Shutdown();
                Application.Current.Shutdown();
            };

            Options.Inst.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == nameof(Options.Inst.Topmost) && Options.Inst.Topmost)
                {
                    Dispatcher?.InvokeAsync(() =>
                    {
                        Thread.Sleep(500);
                        Activate();
                    });
                }
            };

            var exit = new MenuItem {Index = 0, Text = "Quit"};
            exit.Click += (sender, args) => Close();

            var bugReport = new MenuItem {Index = 1, Text = "Send Bug Report"};
            bugReport.Click += (sender, args) => new SendBugReportWindow().Show();

            var resetWindows = new MenuItem {Index = 2, Text = "Reset Layout"};
            resetWindows.Click += (sender, args) => Options.Inst.ResetLayout();

            var support = new MenuItem {Index = 3, Text = "Support on Patreon"};
            support.Click += (sender, args) => Process.Start(new ProcessStartInfo(SettingsControlViewModel.DonationLink));

            using var iconStream = Application.GetResourceStream(new Uri("pack://application:,,,/Crowbars;component/favicon.ico"))?.Stream;
            var trayIcon = new NotifyIcon(new Container())
            {
                Icon = new Icon(iconStream ?? throw new FileNotFoundException()),
                ContextMenu = new ContextMenu {MenuItems = {exit, "-", bugReport, resetWindows, support}},
                Text = $"{Options.Inst.Name} {FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion} by Amaranth",
                Visible = true
            };

            trayIcon.Click += (sender, args) =>
            {
                foreach (var window in Application.Current.Windows.OfType<Window>())
                {
                    window.WindowState = WindowState.Normal;
                }

                Activate();
            };
        }

        private double _jumpedHeight;
        private double _jumpedDelta;
        private DateTime _jumpedTime = DateTime.Now;

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            var delta = sizeInfo.NewSize.Height - sizeInfo.PreviousSize.Height;

            if (Top + sizeInfo.NewSize.Height > SystemParameters.PrimaryScreenHeight)
            {
                if (Top - delta > 0)
                {
                    _jumpedDelta = delta;
                    _jumpedHeight = sizeInfo.PreviousSize.Height;
                    _jumpedTime = DateTime.Now;
                    Top -= _jumpedDelta;
                }
            }
            else if ((DateTime.Now - _jumpedTime).TotalMilliseconds < 200)
            {
                if (Top - delta > 0)
                {
                    _jumpedDelta += delta;
                    Top -= delta;
                    _jumpedTime = DateTime.Now;
                }
            }
            else if (Math.Abs(sizeInfo.NewSize.Height - _jumpedHeight) < 1e-10)
            {
                if (Top + _jumpedDelta < SystemParameters.PrimaryScreenHeight)
                {
                    Top += _jumpedDelta;
                    _jumpedHeight = 0;
                    _jumpedDelta = 0;
                }
            }

            base.OnRenderSizeChanged(sizeInfo);
        }
    }
}